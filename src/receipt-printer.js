export default class ReceiptPrinter {
  constructor (products) {
    this.products = [...products];
  }

  print (barcodes) {
    // TODO: Please implement the method
    // <-start-

    let printedReceipt = '==================== Receipt ====================\n';
    const footer = '\n===================== Total =====================\n';
    let totalPrice = 0;

    if (barcodes.length === 0) {
      const emptyReceipt = `${printedReceipt}\n${footer}${totalPrice.toFixed(2)}`;
      return emptyReceipt;
    }
    // filter unrepeatBarcodes and get all standard barcodes
    const sortedBarcodes = barcodes.sort();
    const unrepeatBarcodes = [...new Set(sortedBarcodes)];
    const allBarcodes = this.products.map(product => product.barcode);
    // use map() to loop the unrepeatBarcode arrays and throw error
    unrepeatBarcodes.map(selectedBarcodes => {
      if (!allBarcodes.includes(selectedBarcodes)) {
        throw Error('Unknown barcode.');
      }
      // get barcodesAmount and filter the products array, then get printedReceipt
      const barcodesAmount = sortedBarcodes.filter(barcode => barcode === selectedBarcodes).length;
      const targetProductArray = this.products.filter(product => product.barcode === selectedBarcodes);
      const targetProduct = targetProductArray[0];
      printedReceipt += `${targetProduct.name.padEnd(30)}x${barcodesAmount}        ${targetProduct.unit}\n`;
      totalPrice += targetProduct.price * barcodesAmount;
    });
    printedReceipt += `${footer}${totalPrice.toFixed(2)}`;
    return printedReceipt;
    // --end->
  }
}
